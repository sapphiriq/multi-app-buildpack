# Heroku Buildpack for Node.js Multiapp repositories

This is the unofficial [Heroku buildpack](http://devcenter.heroku.com/articles/buildpacks) for Node.js apps.

[![Build Status](https://travis-ci.org/heroku/heroku-buildpack-nodejs.svg)](https://travis-ci.org/heroku/heroku-buildpack-nodejs)

## Description

This buildpack allows you to deploy multiple applications from single repository.

### Layout

Modules should be placed at root folder of your repository. Example:

```
-- Root
---- ModuleA
---- ModuleB
---- ModuleC
```

### Heroku setup

Set heroku remotes:

```
heroku git:remote -r MODULE_NAME -a HEROKU_APP_NAME

```

Set config variable HOBOUT_APP to your module name. Variable value and module folder name has to be equal. Example:

```
HOBOUT_APP = ModuleA
```

Set buildpack to:

```
https://bitbucket.org/hobout/multi-app-buildpack.git
```

Push your changes:

```
git push REMOTE_NAME local_brach:master
```